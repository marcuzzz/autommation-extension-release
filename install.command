#!/bin/bash


set -u

if [[ -t 1 ]]
then
  tty_escape() { printf "\033[%sm" "$1"; }
else
  tty_escape() { :; }
fi

tty_mkbold() { tty_escape "1;$1"; }
tty_underline="$(tty_escape "4;39")"
tty_blue="$(tty_mkbold 34)"
tty_red="$(tty_mkbold 31)"
tty_bold="$(tty_mkbold 39)"
tty_reset="$(tty_escape 0)"


# First check OS.
OS="$(uname)"
if [[ "${OS}" == "Linux" ]]
then
  HOMEBREW_ON_LINUX=1
elif [[ "${OS}" != "Darwin" ]]
then
  abort "MMogon is only supported on macOS"
fi


abort() {
  printf "%s\n" "$@" >&2
  exit 1
}

ohai() {
  printf "${tty_blue}==>${tty_bold} %s${tty_reset}\n" "$(shell_join "$@")"
}

shell_join() {
  local arg
  printf "%s" "$1"
  shift
  for arg in "$@"
  do
    printf " "
    printf "%s" "${arg// /\ }"
  done
}


have_sudo_access() {
  if [[ ! -x "/usr/bin/sudo" ]]
  then
    return 1
  fi

  local -a SUDO=("/usr/bin/sudo")
  if [[ -n "${SUDO_ASKPASS-}" ]]
  then
    SUDO+=("-A")
  elif [[ -n "${NONINTERACTIVE-}" ]]
  then
    SUDO+=("-n")
  fi

  if [[ -z "${HAVE_SUDO_ACCESS-}" ]]
  then
    if [[ -n "${NONINTERACTIVE-}" ]]
    then
      "${SUDO[@]}" -l mkdir &>/dev/null
    else
      "${SUDO[@]}" -v && "${SUDO[@]}" -l mkdir &>/dev/null
    fi
    HAVE_SUDO_ACCESS="$?"
  fi

  if [[ -z "${HOMEBREW_ON_LINUX-}" ]] && [[ "${HAVE_SUDO_ACCESS}" -ne 0 ]]
  then
    abort "Need sudo access on macOS (e.g. the user ${USER} needs to be an Administrator)!"
  fi

  return "${HAVE_SUDO_ACCESS}"
}

execute() {
  if ! "$@"
  then
    abort "$(printf "Failed during: %s" "$(shell_join "$@")")"
  fi
}

execute_sudo() {
  local -a args=("$@")
  if have_sudo_access
  then
    if [[ -n "${SUDO_ASKPASS-}" ]]
    then
      args=("-A" "${args[@]}")
    fi
    ohai "/usr/bin/sudo" "${args[@]}"
    execute "/usr/bin/sudo" "${args[@]}"
  else
    ohai "${args[@]}"
    execute "${args[@]}"
  fi
}

ohai "Os: ${OS} & user: ${USER}"
OUTPUT=$(/usr/sbin/spctl --status)

SUB='disable'
if [[ "$OUTPUT" != *"$SUB"* ]]; then
  execute_sudo "/usr/sbin/spctl" "--master-disable"
fi

OUTPUT=$(/usr/sbin/spctl --status)
ohai ${OUTPUT}

# ZIP="/Applications/mmogon.zip"
# if [ -f "$ZIP" ]; then
#   # Take action if $DIR exists. #
#   ohai "Removing old version in ${ZIP}..."
#   execute_sudo "/bin/rm" "-f" "/Applications/mmogon.zip"
# fi

ZIP="/Volumes/GoogleDrive/Shared drives/Monk_Software/03_tools/mmogon/mac/mmogon.zip"
if [ -f "$ZIP" ]; then
  # Take action if $DIR exists. #
  ohai "Copying new version in ${ZIP}..."
  execute_sudo "/bin/cp" "-R" "${ZIP}" "/Applications/"
fi

ZIP="/Volumes/Google Drive/Shared drives/Monk_Software/03_tools/mmogon/mac/mmogon.zip"
if [ -f "$ZIP" ]; then
  # Take action if $DIR exists. #
  ohai "Copying new version in ${ZIP}..."
  execute_sudo "/bin/cp" "-R" "${ZIP}" "/Applications/"
fi

ZIP="/Volumes/GoogleDrive/Unidades\ compartidas/Monk_Software/03_tools/mmogon/mac/mmogon.zip"
if [ -f "$ZIP" ]; then
  # Take action if $DIR exists. #
  ohai "Copying new version in ${ZIP}..."
  execute_sudo "/bin/cp" "-R" "${ZIP}" "/Applications/"
fi

ZIP="/Volumes/Google Drive/Unidades\ compartidas/Monk_Software/03_tools/mmogon/mac/mmogon.zip"
if [ -f "$ZIP" ]; then
  # Take action if $DIR exists. #
  ohai "Copying new version in ${ZIP}..."
  execute_sudo "/bin/cp" "-R" "${ZIP}" "/Applications/"
fi

DIR="/Applications/mmogon"
if [ -d "$DIR" ]; then
  # Take action if $DIR exists. #
  ohai "Removing old version in ${DIR}..."
  execute_sudo "/bin/rm" "-r" "${DIR}"
fi

ZIP="/Applications/mmogon.zip"
if [ -f "$ZIP" ]; then
  # Take action if $DIR exists. #
  ohai "Unzipping ${ZIP}..."
  execute_sudo "/bin/mkdir" "${DIR}"
  execute_sudo "/usr/bin/unzip" "${ZIP}" "-d" "/Applications/mmogon"
else
  ohai "No ${ZIP} found."
  abort "Failed to install mmogon. Please check if you have access to Monk_Software on the Google Shared Drive."
fi

DIR="/Library/Application Support/Adobe/CEP/extensions/com.mmogon-invisible-extension"
if [ -d "$DIR" ]; then
  # Take action if $DIR exists. #
  ohai "Removing old version in ${DIR}..."
  execute_sudo "/bin/rm" "-r" "${DIR}"
fi


DIR="/Library/Application Support/Adobe/CEP/extensions"
ZIP="/Applications/mmogon/tools/com.mmogon-invisible-extension.panel.zxp"
if [ -f "$ZIP" ]; then
	if [ -d "$DIR" ]; then

		execute_sudo "/bin/mkdir" "${DIR}/com.mmogon-invisible-extension"
		execute_sudo "/usr/bin/unzip" "${ZIP}" "-d" "${DIR}/com.mmogon-invisible-extension"

	fi
fi

ohai "Finished!"




